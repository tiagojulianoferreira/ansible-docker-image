
# Ansible build
Constrói imagem Ansible usando o Gitlab CI. A ideia é incluir recursos para tornar a imagem mais versátil e útil para, junto com Gitlab CI, automatizar a orquestração de ambientes e gestão de configurações.

# Casos de uso possíveis

* Gestão de configuração de ativos de rede
* Gestão de configuração e orquestração de servidores/serviços de rede
* Gestão de configuração e orquestração de containers
* Gestão de de configuração e orquestração serviços em nuvem


# Recursos
- [x] Docker in Docker - DIND
- [x] Ansible-galaxy collection para cloud Azure
- [x] Ansible-galaxy collection para cloud AWS
- [x] Dependência Boto3
- [ ] Ansible-galaxy collection para ativos de rede e servidores Dell

# Requisitos
- Implementar um [gitlab-runner](https://docs.gitlab.com/runner/install/) em modo privilegiado

# Exemplo de instalação do Gitlab Runner 

## Cria usuário gitlab-runner e dá permissão no Docker
```shell
sudo useradd gitlab-runner
sudo usermod -aG docker gitlab-runner
sudo -u gitlab-runner -H docker info
```
## Instala mapeando o volume /srv/gitlab-runner/config/
```shell
docker run -d --name gitlab-runner --restart always      -v /srv/gitlab-runner/config:/etc/gitlab-runner      -v /var/run/docker.sock:/var/run/docker.sock      gitlab/gitlab-runner:latest
```

## Para fazer o registro de um novo runner
```shell
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

## Exemplo de config.toml
O arquivo deve ser mapeado em /srv/gitlab-runner/config
```shell
[[runners]]
  name = "gitlab-runner"
  url = "https://gitlab.com/"
  token = "kjskjajjakaLKLSKS="
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```
> Note que tive que editar o arquivo para rodar em modo privilegiado **privileged = true**

# Execução
Para executar o projeto você pode fazer um fork,ajustar as configurações do runner para o seu ambiente e fazer push na **branch build** do repositório, assim o gitlab-ci.yml se encarrega da construção da imagem.

Criei um snippet contendo o início dos passos para utilização da imagem gerada, com um [estudo de caso baseado em gestão de configurações integrando Ansible e Gitlab CI](https://gitlab.com/-/snippets/2246390).


