FROM centos:7

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN curl -sSL https://get.docker.com/ | sh

RUN yum check-update; \
    yum install -y gcc libffi-devel python3 epel-release; \
    yum install -y python3-pip; \
    yum install -y wget; \
    yum clean all

RUN pip3 install --upgrade pip; \
    pip3 install --upgrade virtualenv; \
    pip3 install pywinrm[kerberos]; \
    pip3 install pywinrm; \
    pip3 install jmspath; \
    pip3 install requests; \
    pip3 install boto3;\
    python3 -m pip install ansible; \
    ansible-galaxy collection install azure.azcollection; \
    ansible-galaxy collection install amazon.aws
    # pip3 install -r ~/.ansible/collections/ansible_collections/azure/azcollection/requirements-azure.txt
